# ComplexSet

A collection of reference implementations of the ComplexSet method. 

# What is a ComplexSet

A ComplexSet is an extension of the normal Set datastructure that nearly all major languages define. It holds unique entries in either normal mode or complement mode. In normal mode, the entries are present in the set (the normal behavior of a standard Set). In complement mode, the entries are those which **are not present** in the Set (the opposite behavior of a Set). This enables a ComplexSet to contain information and support for an implicit, undefined, or yet-to-be constructed Universal Set.

Additionally, all ComplexSet implementations provide support for the basic mathematical set operations of unions, intersections, differences, and symmetric differences. All implementations also support copying, switching to/from complement mode, and the relative complement.

# Supported Languages

- Javascript

# Examples

The following reference examples contain the print output of the Javascript implementation.

Let the unknown-to-the-program Universal Set be `{1, 2, 3, 4}`, you can check the output against this implicit universal set.

Let Set A be {1, 2}, (A' = {3,4} or A' = {1, 2}')

Let Set B be {2, 3}, (A' = {1,4} or A' = {2, 3}')

In all examples, assume the following is prepended.

```javascript
let setA = new ComplexSet([1, 2]);
let setB = new ComplexSet([2, 3]);
let CS = ComplexSet;
```

## Example Complements
```
A' => (1,2)' | ComplexSet(2) [Set] { 1, 2, complementMode: true }
B' => (2,3)' | ComplexSet(2) [Set] { 2, 3, complementMode: true }
```

Javascript Code
```javascript
console.log(CS.complement(setA));
console.log(CS.complement(setB));
```

## Example Unions
```
A ⋃ B => (1,2,3) | ComplexSet(3) [Set] { 1, 2, 3, complementMode: false }
A ⋃ B' => (3)'   | ComplexSet(1) [Set] { 3, complementMode: true }
A' ⋃ B => (1)'   | ComplexSet(1) [Set] { 1, complementMode: true }
A' ⋃ B' => (2)'  | ComplexSet(1) [Set] { 2, complementMode: true }
```

Javascript Code
```javascript
console.log(CS.union(setA, setB));
console.log(CS.union(setA, CS.complement(setB)));
console.log(CS.union(CS.complement(setA), setB));
console.log(CS.union(CS.complement(setA), CS.complement(setB)));
```

## Example Intersections
```
A ⋂ B => (2)        | ComplexSet(1) [Set] { 2, complementMode: false }
A ⋂ B' => (1)       | ComplexSet(1) [Set] { 1, complementMode: false }
A' ⋂ B => (3)       | ComplexSet(1) [Set] { 3, complementMode: false }
A' ⋂ B' => (1,2,3)' | ComplexSet(3) [Set] { 1, 2, 3, complementMode: true }
```

Javascript Code
```javascript
console.log(CS.intersection(setA, setB));
console.log(CS.intersection(setA, CS.complement(setB)));
console.log(CS.intersection(CS.complement(setA), setB));
console.log(CS.intersection(CS.complement(setA), CS.complement(setB)));
```

## Example Differences
```
A \ B => (1)       | ComplexSet(1) [Set] { 1, complementMode: false }
A \ B' => (2)      | ComplexSet(1) [Set] { 2, complementMode: false }
A' \ B => (1,2,3)' | ComplexSet(3) [Set] { 1, 2, 3, complementMode: true }
A' \ B' => (3)     | ComplexSet(1) [Set] { 3, complementMode: false }
```

Javascript Code
```javascript
console.log(CS.difference(setA, setB));
console.log(CS.difference(setA, CS.complement(setB)));
console.log(CS.difference(CS.complement(setA), setB));
console.log(CS.difference(CS.complement(setA), CS.complement(setB)));
```
	
## Example Symmetric Differences
```
A Δ B => (1,3)   | ComplexSet(2) [Set] { 1, 3, complementMode: false }
A Δ B' => (1,3)' | ComplexSet(2) [Set] { 1, 3, complementMode: true }
A' Δ B => (1,3)' | ComplexSet(2) [Set] { 1, 3, complementMode: true }
A' Δ B' => (3)   | ComplexSet(2) [Set] { 1, 3, complementMode: false }
```

Javascript Code
```javascript
console.log(CS.symmetricDifference(setA, setB));
console.log(CS.symmetricDifference(setA, CS.complement(setB)));
console.log(CS.symmetricDifference(CS.complement(setA), setB));
console.log(CS.symmetricDifference(CS.complement(setA), CS.complement(setB)));
```

## Relative Complement
```
A R B => (3)       | ComplexSet(1) [Set] { 3, complementMode: false }
A R B' => (1,2,3)' | ComplexSet(3) [Set] { 2, 3, 1, complementMode: true }
A' R B => (2)      | ComplexSet(1) [Set] { 2, complementMode: false }
A' R B' => (1)     | ComplexSet(1) [Set] { 1, complementMode: false }
```

Javascript Code
```javascript
console.log(CS.relativeComplement(setA, setB));
console.log(CS.relativeComplement(setA, CS.complement(setB)));
console.log(CS.relativeComplement(CS.complement(setA), setB));
console.log(CS.relativeComplement(CS.complement(setA), CS.complement(setB)));
```
